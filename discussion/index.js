//CRUD OPERATIONS

//CREATE

//Insert One
/*
	db.collectionName.insertOne({})

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age:21,
	contact: {
		phone:"5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","Javascript","Python"],
	department: "none"
})

//Insert Many
/*
db.users.insertMany([{}])

*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age:76,
		contact: {
			phone:"58878787"
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python","React","PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age:82,
		contact: {
			phone:"5552252"
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React","Laravel","SASS"],
		department: "none"
	}
])


//Read

//Find All

db.users.find()

//Find users with single argument

db.users.find({firstName: "Stephen"})


//Update



db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age:21,
	contact: {
		phone:"5875698"
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","Javascript","Python"],
	department: "none"
})



//Update One

/*

	db.collectionName.updateOne({criteria}, {$set :(field: value)})
	
*/
db.users.updateOne({firstName:"Jane"}, {
	$set: {
		lastName: "Gates",
		age:65,
		contact: {
			phone:"5875698",
			email: "janedoe@gmail.com"
		},
		courses: ["AWS","Google Cloud","Azure"],
		department: "none",
		status: "active"
	}
	
})
db.users.find({firstName: "Jane"})


//Update Many

db.users.updateMany(
	{department:"none"},
	{$set: {department: "hr" }}


	)
db.users.find().pretty()


//Replace One

db.users.replaceOne(
	{lastName: "Gates"},
	{
	
			firstName: "Bill",
			lastName: "Clinton"
		
	}

)

db.users.find({firstName:"Bill"}).pretty()




//Delete
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})

db.users.find({firstName:"Test"})

// Delete one
db.users.deleteOne({firstName:"Test"})


// Delete Many
db.users.deleteMany({courses:[]})

// Delete All
db.users.delete()


// Advanced Queries

db.users.find({
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	}
})

db.users.find({"contact.email" : "janedoe@gmail.com"})


db.users.find({courses: ["React","Laravel","SASS"]})

//Without order
db.users.find({courses:{$all: ["React","Laravel","SASS"]}})



db.users.insert({
	"namearr": [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}

	]
})

